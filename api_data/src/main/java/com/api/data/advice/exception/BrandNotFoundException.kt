package com.api.data.advice.exception

import com.api.data.advice.exception.enumeration.BrandExceptionMessage
import org.springframework.http.HttpStatus

class BrandNotFoundException(
        override val httpStatus: HttpStatus = HttpStatus.BAD_REQUEST,
        messageId: BrandExceptionMessage?
) : GeneralException(httpStatus, messageId?.message!!)