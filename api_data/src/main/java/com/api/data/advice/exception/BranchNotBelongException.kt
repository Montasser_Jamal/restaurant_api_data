package com.api.data.advice.exception

import org.springframework.http.HttpStatus

class BranchNotBelongException(
        override val httpStatus: HttpStatus = HttpStatus.BAD_REQUEST,
        override val message: String = "Branch not belong to this key"
) : GeneralException(httpStatus, message)