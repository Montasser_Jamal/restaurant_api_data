package com.api.data.advice.exception

import org.springframework.http.HttpStatus

class BrandIDNotExistException(
        override val httpStatus: HttpStatus = HttpStatus.CONFLICT,
        override val message: String = "brand-id not exist"
) : GeneralException(httpStatus, message)