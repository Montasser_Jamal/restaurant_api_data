package com.api.data.advice.exception

import org.springframework.http.HttpStatus

class IDNotFoundException(
        override val httpStatus: HttpStatus = HttpStatus.BAD_REQUEST,
        override val message: String = "No id found by this key"
) : GeneralException(httpStatus, message)