package com.api.data.advice.exception

import org.springframework.http.HttpStatus

class BranchNotFoundException(
        override val httpStatus: HttpStatus = HttpStatus.BAD_REQUEST,
        override val message: String = "no branch found by these values"
) : GeneralException(httpStatus, message)