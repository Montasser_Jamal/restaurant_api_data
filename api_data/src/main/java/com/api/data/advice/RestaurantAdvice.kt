package com.api.data.advice

import com.api.data.advice.exception.GeneralException
import com.api.data.model.dto.ExceptionDTO
import org.hibernate.exception.ConstraintViolationException
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.MethodArgumentNotValidException
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.bind.annotation.RestControllerAdvice
import java.util.regex.Pattern

@RestControllerAdvice
class RestaurantAdvice {

    @ExceptionHandler(GeneralException::class)
    fun exceptionCatcher(ex: GeneralException): ResponseEntity<ExceptionDTO> {
        return ResponseEntity.status(ex.httpStatus).body(ExceptionDTO(ex))
    }

    @ExceptionHandler(MethodArgumentNotValidException::class)
    fun badRequestCatcher(ex: MethodArgumentNotValidException): ResponseEntity<ExceptionDTO> {
        val pattern = Pattern.compile("\\{[\\w\\s]+\\}")
        val matcher = pattern.matcher(ex.message!!)

        var message: String? = null
        if (matcher.find()) {
            message = matcher.group(0).replace("[\\{\\}]".toRegex(), "")
        }

        val exception = object : GeneralException(HttpStatus.BAD_REQUEST, message!!) {}

        return ResponseEntity(ExceptionDTO(exception), HttpStatus.BAD_REQUEST)
    }

    @ExceptionHandler(ConstraintViolationException::class)
    fun sqlConstraintViolationCatcher(ex: ConstraintViolationException): ResponseEntity<ExceptionDTO> {
        val message = ex.sqlException.message!!
        val exception = object : GeneralException(HttpStatus.BAD_REQUEST, message) {}
        return ResponseEntity(ExceptionDTO(exception), HttpStatus.BAD_REQUEST)
    }
}