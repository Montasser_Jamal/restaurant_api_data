package com.api.data.advice.exception

import org.springframework.http.HttpStatus

class RecordsNotMatchException(
        override val httpStatus: HttpStatus = HttpStatus.BAD_REQUEST,
        override val message: String = "No record matches these values"
) : GeneralException(httpStatus, message)