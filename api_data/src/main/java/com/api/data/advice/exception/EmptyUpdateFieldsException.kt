package com.api.data.advice.exception

import org.springframework.http.HttpStatus

class EmptyUpdateFieldsException(
        override val httpStatus: HttpStatus = HttpStatus.BAD_REQUEST,
        override val message: String = "No passed fields to update"
) : GeneralException(httpStatus, message)