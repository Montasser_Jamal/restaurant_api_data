package com.api.data.advice.exception

import org.springframework.http.HttpStatus

abstract class GeneralException(
        open val httpStatus: HttpStatus,
        override val message: String
) : RuntimeException()