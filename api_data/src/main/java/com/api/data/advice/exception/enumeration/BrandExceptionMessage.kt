package com.api.data.advice.exception.enumeration

enum class BrandExceptionMessage private constructor(val message: String) {

    NO_BRAND_FOR_THIS_AUTH("No brand found by this key")
}
