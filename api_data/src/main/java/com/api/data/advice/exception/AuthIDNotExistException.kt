package com.api.data.advice.exception

import org.springframework.http.HttpStatus

class AuthIDNotExistException(
        override val httpStatus: HttpStatus = HttpStatus.BAD_REQUEST,
        override val message: String = "auth-id not exist"
) : GeneralException(httpStatus, message)