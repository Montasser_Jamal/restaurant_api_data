package com.api.data.advice.exception

import org.springframework.http.HttpStatus

class DeviceNotBelongException(
        override val httpStatus: HttpStatus = HttpStatus.BAD_REQUEST,
        override val message: String = "Device not belong to this branch"
) : GeneralException(httpStatus, message)