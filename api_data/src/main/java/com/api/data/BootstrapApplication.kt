package com.api.data

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.scheduling.annotation.EnableAsync

@EnableAsync
@SpringBootApplication
open class BootstrapApplication

fun main(args: Array<String>) {
    SpringApplication.run(BootstrapApplication::class.java, *args)
}
