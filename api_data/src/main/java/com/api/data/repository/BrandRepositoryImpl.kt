package com.api.data.repository

import com.api.data.advice.exception.BrandIDNotExistException
import com.api.data.advice.exception.EmptyUpdateFieldsException
import com.api.data.advice.exception.RecordsNotMatchException
import com.api.data.model.entity.BrandEntity
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Repository
import java.math.BigInteger
import javax.persistence.EntityManager
import javax.persistence.PersistenceContext
import javax.persistence.criteria.CriteriaBuilder
import javax.persistence.criteria.CriteriaDelete
import javax.persistence.criteria.CriteriaUpdate
import javax.persistence.criteria.Predicate
import javax.persistence.criteria.Root

@Repository
open class BrandRepositoryImpl : BrandRepository {

    private val LOG = LoggerFactory.getLogger(javaClass)

    @PersistenceContext
    private lateinit var entityManager: EntityManager

    override fun getBrand(brandId: Int?, brandName: String?, authId: Int?): List<BrandEntity> {
        val criteriaBuilder = entityManager.criteriaBuilder

        val query = criteriaBuilder.createQuery(BrandEntity::class.java)
        val root = query.from(BrandEntity::class.java)

        when {
            brandId != null && brandName != null && authId != null -> {
                val brandIdPredicate = criteriaBuilder.equal(root.get<String>(BrandEntity::id.name), brandId)
                val brandNamePredicate = criteriaBuilder.equal(root.get<String>(BrandEntity::name.name), brandName)
                val authIdPredicate = criteriaBuilder.equal(root.get<String>(BrandEntity::authId.name), authId)
                query.where(criteriaBuilder.and(brandIdPredicate, criteriaBuilder.and(brandNamePredicate, authIdPredicate)))
            }

            brandId != null && brandName != null -> {
                val brandIdPredicate = criteriaBuilder.equal(root.get<String>(BrandEntity::id.name), brandId)
                val brandNamePredicate = criteriaBuilder.equal(root.get<String>(BrandEntity::name.name), brandName)
                query.where(criteriaBuilder.and(brandIdPredicate, brandNamePredicate))
            }

            brandId != null && authId != null -> {
                val brandIdPredicate = criteriaBuilder.equal(root.get<String>(BrandEntity::id.name), brandId)
                val authIdPredicate = criteriaBuilder.equal(root.get<String>(BrandEntity::authId.name), authId)
                query.where(criteriaBuilder.and(brandIdPredicate, authIdPredicate))
            }

            brandName != null && authId != null -> {
                val brandNamePredicate = criteriaBuilder.equal(root.get<String>(BrandEntity::name.name), brandName)
                val authIdPredicate = criteriaBuilder.equal(root.get<String>(BrandEntity::authId.name), authId)
                query.where(criteriaBuilder.and(brandNamePredicate, authIdPredicate))
            }

            brandId != null -> query.where(criteriaBuilder.equal(root.get<String>(BrandEntity::id.name), brandId))

            brandName != null -> query.where(criteriaBuilder.equal(root.get<String>(BrandEntity::name.name), brandName))

            authId != null -> query.where(criteriaBuilder.equal(root.get<String>(BrandEntity::authId.name), authId))
        }

        try {
            LOG.info("Retrieve brands")
            return entityManager.createQuery(query).resultList
        } catch (ex: Exception) {
            LOG.error(ex.message)
            throw BrandIDNotExistException()
        }
    }

    override fun addBrand(brandName: String, authId: Int): Int {
        val insert = "insert into brand values(null, :${BrandEntity::name.name}, :${BrandEntity::authId.name})"
        val lastId = "select LAST_INSERT_ID()"

        entityManager.createNativeQuery(insert)
                .setParameter(BrandEntity::name.name, brandName)
                .setParameter(BrandEntity::authId.name, authId)
                .executeUpdate()

        val insertedRowId = entityManager.createNativeQuery(lastId).singleResult as BigInteger
        return insertedRowId.toInt()
    }

    override fun updateBrand(brandId: Int, brandName: String?): Boolean {
        val checks = listOf(brandName).filterNotNull()
        if (checks.isNotEmpty()) {
            val criteriaBuilder: CriteriaBuilder = entityManager.criteriaBuilder

            val update: CriteriaUpdate<BrandEntity> = criteriaBuilder.createCriteriaUpdate(BrandEntity::class.java)
            val rootEntity: Root<BrandEntity> = update.from(BrandEntity::class.java)

            update.set(BrandEntity::name.name, brandName)
            update.where(criteriaBuilder.equal(rootEntity.get<String>(BrandEntity::id.name), brandId))

            val numOfUpdatedRecords = entityManager.createQuery(update).executeUpdate()
            return if (numOfUpdatedRecords != 0) true else throw BrandIDNotExistException()
        } else {
            throw EmptyUpdateFieldsException()
        }
    }

    override fun deleteBrand(brandId: Int?, brandName: String?, authId: Int?): Boolean {
        val criteriaBuilder: CriteriaBuilder = entityManager.criteriaBuilder

        val delete: CriteriaDelete<BrandEntity> = criteriaBuilder.createCriteriaDelete(BrandEntity::class.java)
        val rootEntity: Root<BrandEntity> = delete.from(BrandEntity::class.java)

        val predicates: MutableList<Predicate> = ArrayList()

        if (brandId != null) {
            predicates.add(criteriaBuilder.equal(rootEntity.get<String>(BrandEntity::id.name), brandId))
        }

        if (brandName != null) {
            predicates.add(criteriaBuilder.equal(rootEntity.get<String>(BrandEntity::name.name), brandName))
        }

        if (authId != null) {
            predicates.add(criteriaBuilder.equal(rootEntity.get<String>(BrandEntity::authId.name), authId))
        }

        delete.where(criteriaBuilder.or(*predicates.toTypedArray()))

        val deletedRecords = entityManager.createQuery(delete).executeUpdate();
        return if (deletedRecords != 0) true else throw RecordsNotMatchException()
    }
}