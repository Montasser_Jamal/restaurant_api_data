package com.api.data.repository

import com.api.data.advice.exception.RecordsNotMatchException
import com.api.data.model.entity.RewardsEntity
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Repository
import java.math.BigInteger
import java.util.Objects
import javax.persistence.EntityManager
import javax.persistence.PersistenceContext
import javax.persistence.criteria.Predicate

@Repository
open class RewardRepositoryImpl : RewardRepository {

    private val LOG = LoggerFactory.getLogger(javaClass)

    @PersistenceContext
    private lateinit var entityManager: EntityManager

    override fun getReward(rewardId: Int?, deviceId: String?): List<RewardsEntity> {
        val criteriaBuilder = entityManager.criteriaBuilder

        val query = criteriaBuilder.createQuery(RewardsEntity::class.java)
        val rootEntity = query.from(RewardsEntity::class.java)

        if (listOfNotNull(rewardId, deviceId).isNotEmpty()) {
            if (Objects.nonNull(rewardId)) {
                query.where(criteriaBuilder.equal(rootEntity.get<String>(RewardsEntity::id.name), rewardId))
            }

            if (Objects.nonNull(deviceId)) {
                query.where(criteriaBuilder.equal(rootEntity.get<String>(RewardsEntity::deviceId.name), deviceId))
            }
        }

        LOG.info("Retrieving rewards")
        return entityManager.createQuery(query).resultList
    }

    override fun addReward(deviceId: String, reward: RewardsEntity): Int {
        val insert = "insert into reward values(null, ?, ?, ?, ?)"
        val lastId = "select LAST_INSERT_ID()"

        entityManager.createNativeQuery(insert)
                .setParameter(1, reward.date)
                .setParameter(2, reward.instStoreAvgValue)
                .setParameter(3, reward.accStoreValue)
                .setParameter(4, reward.deviceId)
                .executeUpdate()

        val rowId = entityManager.createNativeQuery(lastId).singleResult as BigInteger
        return rowId.toInt()
    }

    override fun updateReward(rewardId: Int, reward: RewardsEntity): Boolean {
        val criteriaBuilder = entityManager.criteriaBuilder

        val update = criteriaBuilder.createCriteriaUpdate(RewardsEntity::class.java)
        val rootEntity = update.from(RewardsEntity::class.java)

        val field = listOf(reward.date, reward.instStoreAvgValue, reward.accStoreValue, reward.deviceId)
        val fName = listOf(
                RewardsEntity::date,
                RewardsEntity::instStoreAvgValue,
                RewardsEntity::accStoreValue,
                RewardsEntity::deviceId
        )

        for (i in 0 until field.size) {
            if (Objects.nonNull(field[i])) {
                update.set(fName[i].name, field[i])
            }
        }

        update.where(criteriaBuilder.equal(rootEntity.get<String>(RewardsEntity::id.name), rewardId))

        val nOfUpdatedRows = entityManager.createQuery(update).executeUpdate()
        return nOfUpdatedRows > 0
    }

    override fun deleteReward(rewardId: Int?, deviceId: String?): Boolean {
        val criteriaBuilder = entityManager.criteriaBuilder

        val delete = criteriaBuilder.createCriteriaDelete(RewardsEntity::class.java)
        val rootEntity = delete.from(RewardsEntity::class.java)

        val field = listOf(rewardId, deviceId)
        val fName = listOf(RewardsEntity::id, RewardsEntity::deviceId)

        val predicates = mutableListOf<Predicate>()
        for (i in 0 until field.size) {
            if (Objects.nonNull(field[i])) {
                predicates.add(criteriaBuilder.equal(rootEntity.get<String>(fName[i].name), field[i]))
            }
        }

        if (predicates.isNotEmpty()) {
            delete.where(criteriaBuilder.and(*predicates.toTypedArray()))
        }

        val deletedRecords = entityManager.createQuery(delete).executeUpdate();
        return if (deletedRecords != 0) true else throw RecordsNotMatchException()
    }
}