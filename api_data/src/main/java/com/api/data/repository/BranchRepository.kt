package com.api.data.repository

import com.api.data.model.entity.BranchEntity
import org.springframework.transaction.annotation.Transactional

interface BranchRepository {

    fun getBranch(branchId: Int? = null, branchName: String? = null, brandId: Int? = null): List<BranchEntity>

    @Transactional
    fun addBranch(entity: BranchEntity): Boolean

    @Transactional
    fun updateBranch(branchId: Int, entity: BranchEntity): Boolean

    @Transactional
    fun deleteBranch(branchId: Int? = null, branchName: String? = null, brandId: Int? = null): Boolean
}
