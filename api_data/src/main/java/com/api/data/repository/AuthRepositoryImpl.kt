package com.api.data.repository

import com.api.data.advice.exception.AuthIDNotExistException
import com.api.data.advice.exception.RecordsNotMatchException
import com.api.data.model.entity.AuthEntity
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Repository
import java.math.BigInteger
import java.util.Objects
import javax.persistence.EntityManager
import javax.persistence.PersistenceContext
import javax.persistence.criteria.Predicate

@Repository
open class AuthRepositoryImpl : AuthRepository {

    private val LOG = LoggerFactory.getLogger(javaClass)

    @PersistenceContext
    private lateinit var entityManager: EntityManager

    override fun getKeys(authId: Int?, key: String?): List<AuthEntity> {
        val criteriaBuilder = entityManager.criteriaBuilder

        val query = criteriaBuilder.createQuery(AuthEntity::class.java)
        val rootEntity = query.from(AuthEntity::class.java)

        val predicates: MutableList<Predicate> = ArrayList()

        if (Objects.nonNull(authId)) {
            predicates.add(criteriaBuilder.equal(rootEntity.get<String>(AuthEntity::id.name), authId))
        }

        if (Objects.nonNull(key)) {
            predicates.add(criteriaBuilder.equal(rootEntity.get<String>(AuthEntity::key.name), key))
        }

        if (predicates.isNotEmpty()) {
            query.where(criteriaBuilder.or(*predicates.toTypedArray()))
        }

        var resultList: List<AuthEntity> = listOf()
        try {
            LOG.info("Retrieve keys")
            resultList = entityManager.createQuery(query).resultList
        } catch (ex: Exception) {
            LOG.error(ex.message)
        }

        return resultList
    }

    override fun addKey(key: String): Int {
        val insert = "insert into authorization values(null, :key)"
        val lastId = "select LAST_INSERT_ID()"

        entityManager.createNativeQuery(insert)
                .setParameter(AuthEntity::key.name, key)
                .executeUpdate()

        val insertedRowId = entityManager.createNativeQuery(lastId).singleResult as BigInteger
        return insertedRowId.toInt()
    }

    override fun updateKey(authId: Int, key: String): Boolean {
        val criteriaBuilder = entityManager.criteriaBuilder

        val update = criteriaBuilder.createCriteriaUpdate(AuthEntity::class.java)
        val rootEntity = update.from(AuthEntity::class.java)

        update.set(AuthEntity::key.name, key)
        update.where(criteriaBuilder.equal(rootEntity.get<String>(AuthEntity::id.name), authId))

        val numOfUpdatedRecords = entityManager.createQuery(update).executeUpdate()
        return if (numOfUpdatedRecords != 0) true else throw AuthIDNotExistException()
    }

    override fun deleteKeys(authId: Int?, key: String?): Boolean {
        val criteriaBuilder = entityManager.criteriaBuilder

        val delete = criteriaBuilder.createCriteriaDelete(AuthEntity::class.java)
        val rootEntity = delete.from(AuthEntity::class.java)

        val predicates: MutableList<Predicate> = ArrayList()

        if (Objects.nonNull(authId)) {
            predicates.add(criteriaBuilder.equal(rootEntity.get<String>(AuthEntity::id.name), authId))
        }

        if (Objects.nonNull(key)) {
            predicates.add(criteriaBuilder.equal(rootEntity.get<String>(AuthEntity::key.name), key))
        }

        if (predicates.isNotEmpty()) {
            delete.where(criteriaBuilder.and(*predicates.toTypedArray()))
        }

        val deletedRecords = entityManager.createQuery(delete).executeUpdate();
        return if (deletedRecords != 0) true else throw RecordsNotMatchException()
    }
}