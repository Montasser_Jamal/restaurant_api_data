package com.api.data.repository

import com.api.data.model.entity.DeviceEntity
import org.springframework.transaction.annotation.Transactional

interface DeviceRepository {

    fun getDevice(deviceId: String? = null, branchId: Int? = null): List<DeviceEntity>

    @Transactional
    fun addDevice(deviceId: String, branchId: Int): Boolean

    @Transactional
    fun updateDevice(branchId: Int, deviceId: String): Boolean

    @Transactional
    fun deleteDevice(deviceId: String? = null, branchId: Int? = null): Boolean
}