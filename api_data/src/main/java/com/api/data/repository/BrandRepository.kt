package com.api.data.repository

import com.api.data.model.entity.BrandEntity
import org.springframework.transaction.annotation.Transactional

interface BrandRepository {

    fun getBrand(brandId: Int? = null, brandName: String? = null, authId: Int? = null): List<BrandEntity>

    @Transactional
    fun addBrand(brandName: String, authId: Int): Int

    @Transactional
    fun updateBrand(brandId: Int, brandName: String?): Boolean

    @Transactional
    fun deleteBrand(brandId: Int? = null, brandName: String? = null, authId: Int? = null): Boolean
}