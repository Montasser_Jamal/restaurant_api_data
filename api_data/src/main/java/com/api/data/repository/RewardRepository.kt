package com.api.data.repository

import com.api.data.model.entity.RewardsEntity
import org.springframework.transaction.annotation.Transactional

interface RewardRepository {

    fun getReward(rewardId: Int? = null, deviceId: String? = null): List<RewardsEntity>

    @Transactional
    fun addReward(deviceId: String, reward: RewardsEntity): Int

    @Transactional
    fun updateReward(rewardId: Int, reward: RewardsEntity): Boolean

    @Transactional
    fun deleteReward(rewardId: Int?, deviceId: String?): Boolean
}