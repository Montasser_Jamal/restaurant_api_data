package com.api.data.repository

import com.api.data.model.entity.AuthEntity
import org.springframework.transaction.annotation.Transactional

interface AuthRepository {

    fun getKeys(authId: Int? = null, key: String? = null): List<AuthEntity>

    @Transactional
    fun addKey(key: String): Int

    @Transactional
    fun updateKey(authId: Int, key: String): Boolean

    @Transactional
    fun deleteKeys(authId: Int? = null, key: String? = null): Boolean
}