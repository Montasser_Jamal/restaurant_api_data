package com.api.data.repository

import com.api.data.advice.exception.RecordsNotMatchException
import com.api.data.model.entity.DeviceEntity
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Repository
import java.util.Objects
import javax.persistence.EntityManager
import javax.persistence.PersistenceContext
import javax.persistence.criteria.Predicate

@Repository
open class DeviceRepositoryImpl : DeviceRepository {

    private val LOG = LoggerFactory.getLogger(javaClass)

    @PersistenceContext
    private lateinit var entityManager: EntityManager

    override fun getDevice(deviceId: String?, branchId: Int?): List<DeviceEntity> {
        val criteriaBuilder = entityManager.criteriaBuilder

        val query = criteriaBuilder.createQuery(DeviceEntity::class.java)
        val rootEntity = query.from(DeviceEntity::class.java)

        val checks = listOfNotNull(deviceId, branchId)
        if (checks.isNotEmpty()) {
            if (Objects.nonNull(deviceId)) {
                query.where(criteriaBuilder.equal(rootEntity.get<String>(DeviceEntity::id.name), deviceId))
            }

            if (Objects.nonNull(branchId)) {
                query.where(criteriaBuilder.equal(rootEntity.get<String>(DeviceEntity::branchId.name), branchId))
            }
        }

        LOG.info("Retrieving devices")
        return entityManager.createQuery(query).resultList
    }

    override fun addDevice(deviceId: String, branchId: Int): Boolean {
        val insert = "insert into device values(:${DeviceEntity::id.name}, :${DeviceEntity::branchId.name})"

        val result = entityManager.createNativeQuery(insert)
                .setParameter(DeviceEntity::id.name, deviceId)
                .setParameter(DeviceEntity::branchId.name, branchId)
                .executeUpdate()

        return result > 0
    }

    override fun updateDevice(branchId: Int, deviceId: String): Boolean {
        val criteriaBuilder = entityManager.criteriaBuilder

        val update = criteriaBuilder.createCriteriaUpdate(DeviceEntity::class.java)
        val rootEntity = update.from(DeviceEntity::class.java)

        val field = listOf(deviceId)
        val fName = listOf(DeviceEntity::id)
        for (i in 0 until field.size) {
            if (Objects.nonNull(field[i])) {
                update.set(fName[i].name, field[i])
            }
        }

        update.where(criteriaBuilder.equal(rootEntity.get<String>(DeviceEntity::branchId.name), branchId))

        val nOfUpdatedRows = entityManager.createQuery(update).executeUpdate()
        return nOfUpdatedRows > 0
    }

    override fun deleteDevice(deviceId: String?, branchId: Int?): Boolean {
        val criteriaBuilder = entityManager.criteriaBuilder

        val delete = criteriaBuilder.createCriteriaDelete(DeviceEntity::class.java)
        val rootEntity = delete.from(DeviceEntity::class.java)

        val field = listOf(deviceId, branchId)
        val fName = listOf(DeviceEntity::id, DeviceEntity::branchId)

        val predicates = mutableListOf<Predicate>()
        for (i in 0 until field.size) {
            if (Objects.nonNull(field[i])) {
                predicates.add(criteriaBuilder.equal(rootEntity.get<String>(fName[i].name), field[i]))
            }
        }

        if (predicates.isNotEmpty()) {
            delete.where(criteriaBuilder.and(*predicates.toTypedArray()))
        }

        val deletedRecords = entityManager.createQuery(delete).executeUpdate();
        return if (deletedRecords != 0) true else throw RecordsNotMatchException()
    }
}