package com.api.data.repository

import com.api.data.advice.exception.RecordsNotMatchException
import com.api.data.model.entity.BranchEntity
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Repository
import java.util.Objects
import javax.persistence.EntityManager
import javax.persistence.PersistenceContext
import javax.persistence.criteria.Predicate

@Repository
open class BranchRepositoryImpl : BranchRepository {

    private val LOG = LoggerFactory.getLogger(javaClass)

    @PersistenceContext
    private lateinit var entityManager: EntityManager

    override fun getBranch(branchId: Int?, branchName: String?, brandId: Int?): List<BranchEntity> {
        val criteriaBuilder = entityManager.criteriaBuilder

        val query = criteriaBuilder.createQuery(BranchEntity::class.java)
        val rootEntity = query.from(BranchEntity::class.java)

        val field = listOf(branchId, branchName, brandId)
        val fName = listOf(BranchEntity::id, BranchEntity::name, BranchEntity::brandId)

        val predicate = mutableListOf<Predicate>()
        for (i in 0 until field.size) {
            if (Objects.nonNull(field[i])) {
                predicate.add(criteriaBuilder.equal(rootEntity.get<String>(fName[i].name), field[i]))
            }
        }

        if (predicate.isNotEmpty()) {
            query.where(*predicate.toTypedArray())
        }

        LOG.info("Retrieving branches")
        return entityManager.createQuery(query).resultList
    }

    override fun addBranch(entity: BranchEntity): Boolean {
        val insert = "insert into branch values(?, ?, ?)"

        val result = entityManager.createNativeQuery(insert)
                .setParameter(1, entity.id!!)
                .setParameter(2, entity.name!!)
                .setParameter(3, entity.brandId!!)
                .executeUpdate()

        return result > 0
    }

    override fun updateBranch(branchId: Int, entity: BranchEntity): Boolean {
        val criteriaBuilder = entityManager.criteriaBuilder

        val update = criteriaBuilder.createCriteriaUpdate(BranchEntity::class.java)
        val rootEntity = update.from(BranchEntity::class.java)

        val field = listOf(entity.name, entity.brandId)
        val fName = listOf(BranchEntity::name, BranchEntity::brandId)
        for (i in 0 until field.size) {
            if (Objects.nonNull(field[i])) {
                update.set(fName[i].name, field[i])
            }
        }

        update.where(criteriaBuilder.equal(rootEntity.get<String>(BranchEntity::id.name), branchId))

        val nOfUpdatedRows = entityManager.createQuery(update).executeUpdate()
        return nOfUpdatedRows > 0
    }

    override fun deleteBranch(branchId: Int?, branchName: String?, brandId: Int?): Boolean {
        val criteriaBuilder = entityManager.criteriaBuilder

        val delete = criteriaBuilder.createCriteriaDelete(BranchEntity::class.java)
        val rootEntity = delete.from(BranchEntity::class.java)

        val field = listOf(branchId, branchName, brandId)
        val fName = listOf(BranchEntity::id, BranchEntity::name, BranchEntity::brandId)

        val predicates = mutableListOf<Predicate>()
        for (i in 0 until field.size) {
            if (Objects.nonNull(field[i])) {
                predicates.add(criteriaBuilder.equal(rootEntity.get<String>(fName[i].name), field[i]))
            }
        }

        if (predicates.isNotEmpty()) {
            delete.where(criteriaBuilder.and(*predicates.toTypedArray()))
        }

        val deletedRecords = entityManager.createQuery(delete).executeUpdate();
        return if (deletedRecords != 0) true else throw RecordsNotMatchException()
    }
}
