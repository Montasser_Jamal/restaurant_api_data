package com.api.data.model.dto

import com.api.data.model.wrapper.RewardWrapper

data class RewardDTO(
        private val wrapper: RewardWrapper
) : GeneralDTO<RewardWrapper>(data = wrapper)