package com.api.data.model.wrapper

import com.api.data.model.serialize.classes.BranchResponse

data class BranchWrapper(val branches: List<BranchResponse>)