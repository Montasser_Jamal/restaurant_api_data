package com.api.data.model.entity

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Id

@Entity(name = "branch")
class BranchEntity {

    @Id
    var id: Int? = null

    var name: String? = null

    @Column(name = "brand_id")
    var brandId: Int? = null

    constructor() //Empty Constructor

    constructor(id: Int? = null, name: String?, brandId: Int?) {
        this.id = id
        this.name = name
        this.brandId = brandId
    }
}