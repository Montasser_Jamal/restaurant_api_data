package com.api.data.model.mapper

data class DeviceMapper(
        var deviceId: String?,
        var rewards: MutableList<RewardMapper>?
)