package com.api.data.model.serialize.classes

import com.fasterxml.jackson.annotation.JsonProperty

data class BranchResponse(
        @JsonProperty("branch_id")
        var branchId: Int,
        @JsonProperty("branch_name")
        var branchName: String
)
