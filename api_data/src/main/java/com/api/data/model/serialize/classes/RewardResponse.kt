package com.api.data.model.serialize.classes

import com.api.data.model.entity.RewardsEntity
import com.fasterxml.jackson.annotation.JsonProperty
import java.time.LocalDateTime

data class RewardResponse(
        @JsonProperty("reward-id")
        val rewardId: Int? = null,
        val date: LocalDateTime? = null,
        @JsonProperty("inst-store-avg-value")
        val instStoreAvgValue: Int? = null,
        @JsonProperty("acc-store-value")
        val accStoreValue: Double? = null
) {
    operator fun plus(entity: RewardsEntity): RewardResponse = RewardResponse(
            date = entity.date,
            instStoreAvgValue = entity.instStoreAvgValue,
            accStoreValue = entity.accStoreValue
    )
}