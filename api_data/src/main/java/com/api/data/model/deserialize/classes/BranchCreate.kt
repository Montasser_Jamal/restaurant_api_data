package com.api.data.model.deserialize.classes

import com.fasterxml.jackson.annotation.JsonProperty
import org.springframework.validation.annotation.Validated
import javax.validation.constraints.NotEmpty
import javax.validation.constraints.NotNull
import javax.validation.constraints.Positive

@Validated
class BranchCreate {

    @JsonProperty("branch-id")
    @Positive(message = "{Branch id must be a positive number}")
    @NotNull(message = "{Branch id must be not null}")
    var branchId: Int? = null

    @JsonProperty("branch-name")
    @NotEmpty(message = "{Branch name must be not empty}")
    @NotNull(message = "{Branch name must be not null}")
    var branchName: String? = null

    constructor()

    constructor(branchId: Int?, branchName: String?) {
        this.branchId = branchId
        this.branchName = branchName
    }
}