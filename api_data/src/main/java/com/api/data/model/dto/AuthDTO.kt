package com.api.data.model.dto

import com.api.data.model.wrapper.AuthWrapper

data class AuthDTO(
        private val wrapper: AuthWrapper
) : GeneralDTO<AuthWrapper>(data = wrapper)