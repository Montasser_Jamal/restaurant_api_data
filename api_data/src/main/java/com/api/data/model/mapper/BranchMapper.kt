package com.api.data.model.mapper

data class BranchMapper(
        var branchId: Int?,
        var devices: List<DeviceMapper>?
)