package com.api.data.model.serialize.classes

import com.fasterxml.jackson.annotation.JsonProperty

data class DeviceResponse(
        @JsonProperty("device-id")
        val deviceId: String
)