package com.api.data.model.serialize.classes

import com.api.data.model.entity.BrandEntity
import com.fasterxml.jackson.annotation.JsonProperty

data class BrandResponse(
        @JsonProperty("brand-id")
        val brandId: Int? = null,
        @JsonProperty("brand-name")
        val brandName: String? = null,
        @JsonProperty("auth-id")
        val authId: Int? = null
) {
    operator fun plus(entity: BrandEntity): BrandResponse = BrandResponse(entity.id!!, entity.name!!, entity.authId!!)
}