package com.api.data.model.dto

import com.api.data.advice.exception.GeneralException

data class ExceptionDTO(private val ex: GeneralException) {

    val message: String

    init {
        this.message = ex.message
    }
}