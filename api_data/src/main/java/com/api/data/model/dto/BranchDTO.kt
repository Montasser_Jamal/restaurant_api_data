package com.api.data.model.dto

import com.api.data.model.wrapper.BranchWrapper

data class BranchDTO(
        private val wrapper: BranchWrapper
) : GeneralDTO<BranchWrapper>(data = wrapper)