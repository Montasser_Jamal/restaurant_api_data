package com.api.data.model.dto

abstract class GeneralDTO<T>(
        header: ArrayList<String>? = null,
        var data: T?
)