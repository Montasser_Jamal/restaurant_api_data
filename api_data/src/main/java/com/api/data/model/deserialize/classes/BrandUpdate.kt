package com.api.data.model.deserialize.classes

import com.fasterxml.jackson.annotation.JsonProperty
import org.springframework.validation.annotation.Validated
import javax.validation.constraints.NotEmpty

@Validated
class BrandUpdate {

    @JsonProperty("brand-name")
    @NotEmpty(message = "{Brand name must be not empty}")
    var brandName: String? = null

    constructor()

    constructor(brandName: String?) {
        this.brandName = brandName
    }
}