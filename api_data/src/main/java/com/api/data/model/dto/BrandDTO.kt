package com.api.data.model.dto

import com.api.data.model.wrapper.BrandWrapper

data class BrandDTO(
        private val response: BrandWrapper
) : GeneralDTO<BrandWrapper>(data = response)