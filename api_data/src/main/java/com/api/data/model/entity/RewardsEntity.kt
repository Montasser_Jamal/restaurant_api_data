package com.api.data.model.entity

import java.time.LocalDateTime
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id

@Entity(name = "reward")
class RewardsEntity {

    @Id
    @GeneratedValue
    var id: Int? = null

    var date: LocalDateTime? = null

    @Column(name = "inst_store_avg_value")
    var instStoreAvgValue: Int? = null

    @Column(name = "acc_store_value")
    var accStoreValue: Double? = null

    @Column(name = "device_id")
    var deviceId: String? = null

    constructor() //Empty Constructor

    constructor(id: Int? = null, date: LocalDateTime?, instStoreAvgValue: Int?, accStoreValue: Double?, deviceId: String?) {
        this.id = id
        this.date = date
        this.instStoreAvgValue = instStoreAvgValue
        this.accStoreValue = accStoreValue
        this.deviceId = deviceId
    }
}
