package com.api.data.model.deserialize.classes

import com.fasterxml.jackson.annotation.JsonProperty
import org.springframework.validation.annotation.Validated
import javax.validation.constraints.NotEmpty
import javax.validation.constraints.NotNull
import javax.validation.constraints.Positive

@Validated
class BrandCreate {

    @JsonProperty("brand-name")
    @NotEmpty(message = "{Brand name must be not empty}")
    @NotNull(message = "{Brand name must be not null}")
    var brandName: String? = null

    @JsonProperty("auth-id")
    @Positive(message = "{Auth id must be a positive number}")
    @NotNull(message = "{Auth id must be not null}")
    var authId: Int? = null

    constructor()

    constructor(brandName: String?, authId: Int?) {
        this.brandName = brandName
        this.authId = authId
    }
}