package com.api.data.model.wrapper

import com.api.data.model.serialize.classes.DeviceResponse

data class DeviceWrapper(val devices: List<DeviceResponse>)