package com.api.data.model.entity

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id

@Entity(name = "brand")
class BrandEntity {

    @Id
    @GeneratedValue
    var id: Int? = null

    var name: String? = null

    @Column(name = "auth_id")
    var authId: Int? = null

    constructor() //Empty Constructor

    constructor(id: Int?, name: String?) {
        this.id = id
        this.name = name
    }

    constructor(id: Int? = null, name: String?, authId: Int?) {
        this.id = id
        this.name = name
        this.authId = authId
    }
}