package com.api.data.model.serialize.classes

import com.fasterxml.jackson.annotation.JsonProperty

data class IDResponse(
        @JsonProperty("auth-id")
        val authId: Int? = null
)