package com.api.data.model.deserialize.classes

import com.fasterxml.jackson.annotation.JsonProperty
import org.springframework.validation.annotation.Validated
import javax.validation.constraints.NotEmpty
import javax.validation.constraints.NotNull

@Validated
class AuthUpdate {

    @JsonProperty("user-name")
    @NotEmpty(message = "{Username must be not empty}")
    @NotNull(message = "{Username must be not null}")
    var userName: String? = null

    @JsonProperty("password")
    @NotEmpty(message = "{Password must be not empty}")
    @NotNull(message = "{Password must be not null}")
    var password: String? = null

    constructor()

    constructor(userName: String?, password: String?) {
        this.userName = userName
        this.password = password
    }
}