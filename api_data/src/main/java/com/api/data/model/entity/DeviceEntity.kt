package com.api.data.model.entity

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Id

@Entity(name = "device")
class DeviceEntity {

    @Id
    var id: String? = null

    @Column(name = "branch_id")
    var branchId: Int? = null

    constructor() //Empty Constructor

    constructor(id: String?, branchId: Int?) {
        this.id = id
        this.branchId = branchId
    }
}