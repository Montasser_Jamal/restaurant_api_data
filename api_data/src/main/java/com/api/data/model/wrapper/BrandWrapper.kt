package com.api.data.model.wrapper

import com.api.data.model.serialize.classes.BrandResponse

data class BrandWrapper(val brands: List<BrandResponse>)