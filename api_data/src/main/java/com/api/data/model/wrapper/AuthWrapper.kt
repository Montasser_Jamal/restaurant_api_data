package com.api.data.model.wrapper

import com.api.data.model.serialize.classes.AuthResponse

data class AuthWrapper(val auth: List<AuthResponse>)