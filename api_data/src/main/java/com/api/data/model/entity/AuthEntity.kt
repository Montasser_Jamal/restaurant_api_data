package com.api.data.model.entity

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id

@Entity(name = "authorization")
class AuthEntity {

    @Id
    @GeneratedValue
    var id: Int? = null

    @Column(name = "gen_key")
    var key: String? = null

    constructor() //Empty constructor

    constructor(id: Int? = null, key: String?) {
        this.id = id
        this.key = key
    }
}