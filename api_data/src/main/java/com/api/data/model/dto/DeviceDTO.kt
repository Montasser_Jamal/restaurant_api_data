package com.api.data.model.dto

import com.api.data.model.wrapper.DeviceWrapper

data class DeviceDTO(
        private val wrapper: DeviceWrapper
) : GeneralDTO<DeviceWrapper>(data = wrapper)