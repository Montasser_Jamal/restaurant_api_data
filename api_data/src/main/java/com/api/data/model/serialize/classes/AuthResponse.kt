package com.api.data.model.serialize.classes

import com.api.data.model.entity.AuthEntity
import com.fasterxml.jackson.annotation.JsonProperty

data class AuthResponse(
        @JsonProperty("auth-id")
        val authId: Int? = null,
        @JsonProperty("key")
        val key: String? = null
) {
    operator fun plus(entity: AuthEntity): AuthResponse = AuthResponse(entity.id!!, entity.key!!)
}