package com.api.data.model.mapper

import java.time.LocalDateTime

data class RewardMapper(
        var date: LocalDateTime,
        var instStoreAvgValue: Int?,
        var accStoreValue: Double?
)