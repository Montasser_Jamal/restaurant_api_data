package com.api.data.model.wrapper

import com.api.data.model.serialize.classes.RewardResponse

data class RewardWrapper(val rewards: List<RewardResponse>)