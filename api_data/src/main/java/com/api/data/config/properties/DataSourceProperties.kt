package com.api.data.config.properties

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.stereotype.Component

@Component
@ConfigurationProperties("datasource")
class DataSourceProperties {

    lateinit var url: String

    lateinit var userName: String

    lateinit var password: String
}
