package com.api.data.config.properties

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.stereotype.Component

@Component
@ConfigurationProperties("external.api.request")
class ApiRequestProperties {

    lateinit var url: String
}
