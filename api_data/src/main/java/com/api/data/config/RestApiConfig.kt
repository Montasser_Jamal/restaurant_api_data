package com.api.data.config

import com.api.data.api.AuthRest
import com.api.data.api.AuthRestImpl
import com.api.data.api.BranchRest
import com.api.data.api.BranchRestImpl
import com.api.data.api.BrandRest
import com.api.data.api.BrandRestImpl
import com.api.data.service.handler.AuthRestHandler
import com.api.data.service.handler.BranchRestHandler
import com.api.data.service.handler.BrandRestHandler
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
open class RestApiConfig {

    @Bean
    open fun brandEndpoints(handler: BrandRestHandler): BrandRest = BrandRestImpl(handler)

    @Bean
    open fun authEndpoints(handler: AuthRestHandler): AuthRest = AuthRestImpl(handler)

    @Bean
    open fun branchEndpoints(handler: BranchRestHandler): BranchRest = BranchRestImpl(handler)
}