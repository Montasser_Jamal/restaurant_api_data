package com.api.data.config

import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.SerializationFeature
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
open class SerializeDeserializeConfig {

    @Bean
    open fun objectMapper(mapper: ObjectMapper): ObjectMapper {
        //When enable WRAP_ROOT_VALUE it causes a swagger-ui problem
        mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false)
        mapper.configure(DeserializationFeature.UNWRAP_ROOT_VALUE, false)
        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL)
        return mapper
    }
}