package com.api.data.job

import com.api.data.model.entity.RewardsEntity
import com.api.data.repository.AuthRepository
import com.api.data.repository.BranchRepository
import com.api.data.repository.BrandRepository
import com.api.data.repository.DeviceRepository
import com.api.data.repository.RewardRepository
import com.api.data.service.LinkersService
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.scheduling.annotation.EnableScheduling
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Component
import java.time.LocalDateTime
import java.util.Objects

@EnableScheduling
@Component
class BranchDataJob @Autowired constructor(
        private val authRepository: AuthRepository,
        private val brandRepository: BrandRepository,
        private val branchRepository: BranchRepository,
        private val deviceRepository: DeviceRepository,
        private val rewardRepository: RewardRepository,
        private val linkersService: LinkersService
) {

    private val LOG = LoggerFactory.getLogger(javaClass)

    @Scheduled(cron = "* * 0 * * *")
    fun getBranchData() {
        LOG.info("Start scheduling at {}", LocalDateTime.now())

        val authEntities = authRepository.getKeys()
        for (auth in authEntities) {
            val brandEntities = brandRepository.getBrand(authId = auth.id)
            for (brand in brandEntities) {
                val branchEntities = branchRepository.getBranch(brandId = brand.id)
                for (branch in branchEntities) {
                    val rewards = linkersService.getRewards(auth.key!!, branch.id!!)
                    for (device in rewards) {
                        try {
                            deviceRepository.addDevice(device.deviceId!!, branch.id!!)
                        } catch (ex: Exception) {
                            //Empty catch
                        }

                        if (Objects.nonNull(device.rewards)) {
                            for (reward in device.rewards!!) {
                                val entity = RewardsEntity(
                                        date = reward.date,
                                        instStoreAvgValue = reward.instStoreAvgValue,
                                        accStoreValue = reward.accStoreValue,
                                        deviceId = device.deviceId
                                )

                                try {
                                    rewardRepository.addReward(device.deviceId!!, entity)
                                } catch (ex: Exception) {
                                    //Empty catch
                                }
                            }
                        }
                    }
                }
            }
        }

        LOG.info("End scheduling at {}", LocalDateTime.now())
    }
}
