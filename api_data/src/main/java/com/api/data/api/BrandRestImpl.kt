package com.api.data.api

import com.api.data.model.deserialize.classes.BrandCreate
import com.api.data.model.deserialize.classes.BrandUpdate
import com.api.data.model.dto.BrandDTO
import com.api.data.model.serialize.classes.IDResponse
import com.api.data.service.handler.BrandRestHandler
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity

open class BrandRestImpl @Autowired constructor(private val HANDLER: BrandRestHandler) : BrandRest {

    override fun getBrands(brandId: Int?, brandName: String?, authId: Int?): ResponseEntity<BrandDTO> {
        val wrapper = HANDLER.getBrands(brandId, brandName, authId)

        return ResponseEntity(BrandDTO(wrapper), HttpStatus.OK)
    }

    override fun addNewBrand(create: BrandCreate): ResponseEntity<IDResponse> {
        val response = HANDLER.addBrand(create)
        return ResponseEntity(response, HttpStatus.CREATED)
    }

    override fun updateBrand(brandId: Int, update: BrandUpdate): ResponseEntity<Void> {
        val isUpdated = HANDLER.updateBrand(brandId, update)

        return ResponseEntity(HttpStatus.OK)
    }

    override fun deleteBrands(brandId: Int?, brandName: String?, authId: Int?): ResponseEntity<Void> {
        val isDeleted = HANDLER.deleteBrands(brandId, brandName, authId)

        return ResponseEntity(HttpStatus.OK)
    }
}