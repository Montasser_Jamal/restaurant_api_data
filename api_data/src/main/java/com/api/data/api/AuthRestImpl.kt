package com.api.data.api

import com.api.data.model.deserialize.classes.AuthCreate
import com.api.data.model.deserialize.classes.AuthUpdate
import com.api.data.model.dto.AuthDTO
import com.api.data.model.serialize.classes.IDResponse
import com.api.data.service.handler.AuthRestHandler
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity

open class AuthRestImpl @Autowired constructor(private val HANDLER: AuthRestHandler) : AuthRest {

    override fun getKeys(authId: Int?, key: String?): ResponseEntity<AuthDTO> {
        val wrapper = HANDLER.getKeys(authId, key)

        return ResponseEntity.ok(AuthDTO(wrapper))
    }

    override fun addNewBrand(create: AuthCreate): ResponseEntity<IDResponse> {
        val response = HANDLER.addKey(create)
        return ResponseEntity(response, HttpStatus.CREATED)
    }

    override fun updateBrand(authId: Int, update: AuthUpdate): ResponseEntity<Void> {
        val isUpdated = HANDLER.updateKey(authId, update)
        return ResponseEntity.ok().build()
    }

    override fun deleteBrands(authId: Int?, key: String?): ResponseEntity<Void> {
        val isDeleted = HANDLER.deleteKeys(authId, key)
        return ResponseEntity.ok().build()
    }
}