package com.api.data.api

import com.api.data.model.deserialize.classes.AuthCreate
import com.api.data.model.deserialize.classes.AuthUpdate
import com.api.data.model.dto.AuthDTO
import com.api.data.model.serialize.classes.IDResponse
import io.swagger.annotations.ApiOperation
import org.springframework.http.ResponseEntity
import org.springframework.validation.annotation.Validated
import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PatchMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import javax.validation.Valid

@Validated
@RestController
@RequestMapping("/keys")
interface AuthRest {

    @ApiOperation("Get keys")
    @GetMapping
    fun getKeys(
            @RequestParam(name = "auth-id", required = false) authId: Int?,
            @RequestParam(name = "key", required = false) key: String?
    ): ResponseEntity<AuthDTO>

    @ApiOperation("Add new key")
    @PostMapping
    fun addNewBrand(
            @Valid
            @RequestBody create: AuthCreate
    ): ResponseEntity<IDResponse>

    @ApiOperation("Update key")
    @PatchMapping
    fun updateBrand(
            @RequestParam("auth-id") authId: Int,
            @Valid
            @RequestBody update: AuthUpdate
    ): ResponseEntity<Void>

    @ApiOperation("Delete keys")
    @DeleteMapping
    fun deleteBrands(
            @RequestParam("auth-id", required = false) authId: Int?,
            @RequestParam("key", required = false) key: String?
    ): ResponseEntity<Void>

}
