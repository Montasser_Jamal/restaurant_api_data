package com.api.data.api

import com.api.data.model.deserialize.classes.BranchCreate
import com.api.data.model.dto.BranchDTO
import com.api.data.model.dto.DeviceDTO
import com.api.data.model.dto.RewardDTO
import com.api.data.service.handler.BranchRestHandler
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity

open class BranchRestImpl @Autowired constructor(private val HANDLER: BranchRestHandler) : BranchRest {

    override fun getBranches(auth: String): ResponseEntity<BranchDTO> {
        val wrapper = HANDLER.getBranch(auth)
        return ResponseEntity.ok(BranchDTO(wrapper))
    }

    override fun getDevices(auth: String, branchId: Int): ResponseEntity<DeviceDTO> {
        val wrapper = HANDLER.getDevice(auth, branchId)
        return ResponseEntity.ok(DeviceDTO(wrapper))
    }

    override fun getRewards(auth: String, branchId: Int, deviceId: String): ResponseEntity<RewardDTO> {
        val wrapper = HANDLER.getReward(auth, branchId, deviceId)
        return ResponseEntity.ok(RewardDTO(wrapper))
    }

    override fun addNewBranch(auth: String, create: BranchCreate): ResponseEntity<Void> {
        HANDLER.addBranch(auth, create)
        return ResponseEntity(HttpStatus.CREATED)
    }
}
