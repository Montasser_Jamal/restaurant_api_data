package com.api.data.api

import com.api.data.model.deserialize.classes.BrandCreate
import com.api.data.model.deserialize.classes.BrandUpdate
import com.api.data.model.dto.BrandDTO
import com.api.data.model.serialize.classes.IDResponse
import io.swagger.annotations.ApiOperation
import org.springframework.http.ResponseEntity
import org.springframework.validation.annotation.Validated
import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PatchMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import javax.validation.Valid

@Validated
@RestController
@RequestMapping("/brands")
interface BrandRest {

    @ApiOperation("Get brands")
    @GetMapping
    fun getBrands(
            @RequestParam(name = "brand-id", required = false) brandId: Int?,
            @RequestParam(name = "brand-name", required = false) brandName: String?,
            @RequestParam(name = "auth-id", required = false) authId: Int?
    ): ResponseEntity<BrandDTO>

    @ApiOperation("Add new brand")
    @PostMapping
    fun addNewBrand(
            @Valid
            @RequestBody create: BrandCreate
    ): ResponseEntity<IDResponse>

    @ApiOperation("Update brand name")
    @PatchMapping
    fun updateBrand(
            @RequestParam("brand-id") brandId: Int,
            @Valid
            @RequestBody update: BrandUpdate
    ): ResponseEntity<Void>

    @ApiOperation("Delete brands")
    @DeleteMapping
    fun deleteBrands(
            @RequestParam("brand-id", required = false) brandId: Int?,
            @RequestParam("brand-name", required = false) brandName: String?,
            @RequestParam("auth-id", required = false) authId: Int?
    ): ResponseEntity<Void>

}
