package com.api.data.api

import com.api.data.model.deserialize.classes.BranchCreate
import com.api.data.model.dto.BranchDTO
import com.api.data.model.dto.DeviceDTO
import com.api.data.model.dto.RewardDTO
import io.swagger.annotations.ApiOperation
import org.springframework.http.HttpHeaders
import org.springframework.http.ResponseEntity
import org.springframework.validation.annotation.Validated
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestHeader
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import javax.validation.Valid

@Validated
@RestController
@RequestMapping(
        path = ["/branches"],
        headers = [HttpHeaders.AUTHORIZATION]
)
interface BranchRest {

    @ApiOperation("Get branches")
    @GetMapping
    fun getBranches(
            @RequestHeader(HttpHeaders.AUTHORIZATION) auth: String
    ): ResponseEntity<BranchDTO>

    @ApiOperation("Get devices")
    @GetMapping("/{branch-id}/devices")
    fun getDevices(
            @RequestHeader(HttpHeaders.AUTHORIZATION) auth: String,
            @PathVariable("branch-id") branchId: Int
    ): ResponseEntity<DeviceDTO>

    @ApiOperation("Get rewards")
    @GetMapping("/{branch-id}/devices/{device-id}/rewards")
    fun getRewards(
            @RequestHeader(HttpHeaders.AUTHORIZATION) auth: String,
            @PathVariable("branch-id") branchId: Int,
            @PathVariable("device-id") deviceId: String
    ): ResponseEntity<RewardDTO>

    @ApiOperation("Add branch")
    @PostMapping
    fun addNewBranch(
            @RequestHeader(HttpHeaders.AUTHORIZATION) auth: String,
            @Valid
            @RequestBody create: BranchCreate
    ): ResponseEntity<Void>
}