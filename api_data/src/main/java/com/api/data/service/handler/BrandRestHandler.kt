package com.api.data.service.handler

import com.api.data.model.deserialize.classes.BrandCreate
import com.api.data.model.deserialize.classes.BrandUpdate
import com.api.data.model.serialize.classes.IDResponse
import com.api.data.model.wrapper.BrandWrapper
import com.api.data.service.service.BrandService
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component

@Component
class BrandRestHandler @Autowired constructor(private val BRAND_SERVICE: BrandService) {

    private val LOG = LoggerFactory.getLogger(javaClass)

    fun getBrands(brandId: Int?, brandName: String?, authId: Int?): BrandWrapper {
        val brands = BRAND_SERVICE.getBrands(brandId, brandName?.toLowerCase(), authId)

        return BrandWrapper(brands)
    }

    fun addBrand(create: BrandCreate): IDResponse {
        val brandId = BRAND_SERVICE.addBrand(create.brandName!!, create.authId!!)
        return IDResponse(brandId)
    }

    fun updateBrand(brandId: Int, update: BrandUpdate): Boolean {
        return BRAND_SERVICE.updateBrand(
                brandId,
                update.brandName
        )
    }

    fun deleteBrands(brandId: Int?, brandName: String?, authId: Int?): Boolean {
        return BRAND_SERVICE.deleteBrands(brandId, brandName, authId)
    }
}