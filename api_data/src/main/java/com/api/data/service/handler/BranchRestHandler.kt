package com.api.data.service.handler

import com.api.data.model.deserialize.classes.BranchCreate
import com.api.data.model.wrapper.BranchWrapper
import com.api.data.model.wrapper.DeviceWrapper
import com.api.data.model.wrapper.RewardWrapper
import com.api.data.service.service.BranchService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component

@Component
class BranchRestHandler @Autowired constructor(private val BRANCH_SERVICE: BranchService) {

    fun getBranch(authKey: String? = null, branchId: Int? = null, branchName: String? = null): BranchWrapper {
        val branches = BRANCH_SERVICE.getBranch(authKey, branchId, branchName)
        return BranchWrapper(branches)
    }

    fun addBranch(authKey: String, create: BranchCreate): Boolean {
        return BRANCH_SERVICE.addBranch(authKey, create.branchId!!, create.branchName!!)
    }

    fun getDevice(authKey: String, branchId: Int): DeviceWrapper {
        val devices = BRANCH_SERVICE.getDevice(authKey, branchId)
        return DeviceWrapper(devices)
    }

    fun getReward(authKey: String, branchId: Int, deviceId: String): RewardWrapper {
        val rewards = BRANCH_SERVICE.getReward(authKey, branchId, deviceId)
        return RewardWrapper(rewards)
    }
}
