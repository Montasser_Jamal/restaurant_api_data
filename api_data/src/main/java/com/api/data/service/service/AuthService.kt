package com.api.data.service.service

import com.api.data.model.serialize.classes.AuthResponse
import com.api.data.repository.AuthRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
open class AuthService @Autowired constructor(private val REPOSITORY: AuthRepository) {

    fun getKeys(authId: Int?, key: String?): List<AuthResponse> = REPOSITORY.getKeys(authId, key).map {
        AuthResponse() + it
    }

    fun addKey(key: String): Int = REPOSITORY.addKey(key)

    fun updateKey(authId: Int, key: String): Boolean = REPOSITORY.updateKey(authId, key)

    fun deleteKeys(authId: Int?, key: String?): Boolean = REPOSITORY.deleteKeys(authId, key)
}