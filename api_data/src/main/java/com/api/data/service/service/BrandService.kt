package com.api.data.service.service

import com.api.data.model.serialize.classes.BrandResponse
import com.api.data.repository.BrandRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
open class BrandService @Autowired constructor(private val REPOSITORY: BrandRepository) {

    fun getBrands(brandId: Int?, brandName: String?, authId: Int?): List<BrandResponse> {
        return REPOSITORY.getBrand(brandId, brandName, authId).map {
            BrandResponse() + it
        }
    }

    fun addBrand(brandName: String, authId: Int): Int {
        return REPOSITORY.addBrand(brandName, authId)
    }

    fun updateBrand(brandId: Int, brandName: String?): Boolean {
        return REPOSITORY.updateBrand(brandId, brandName)
    }

    fun deleteBrands(brandId: Int?, brandName: String?, authId: Int?): Boolean {
        return REPOSITORY.deleteBrand(brandId, brandName, authId)
    }
}