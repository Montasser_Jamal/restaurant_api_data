package com.api.data.service

import com.api.data.config.properties.ApiRequestProperties
import com.api.data.model.mapper.DeviceMapper
import com.api.data.model.mapper.RewardMapper
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpEntity
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpMethod
import org.springframework.stereotype.Service
import org.springframework.web.client.RestTemplate
import java.time.LocalDateTime
import java.util.Objects

@Service
open class LinkersService @Autowired constructor(
        private val template: RestTemplate,
        private val requestConfig: ApiRequestProperties
) {

    private val log = LoggerFactory.getLogger(javaClass)

    open fun getRewards(authorization: String, branchNum: Int): List<DeviceMapper> {
        val devices = ArrayList<DeviceMapper>()

        val url = String.format(requestConfig.url, branchNum)
        var branchRewards: Map<*, *>? = null
        try {
            val headers = HttpHeaders()
            headers.set("Authorization", "Basic $authorization")

            val entity = HttpEntity<HttpHeaders>(headers)

            val responseEntity = template.exchange(url, HttpMethod.GET, entity, Map::class.java)
            branchRewards = responseEntity.body
        } catch (ex: Exception) {
            log.error("Request external api failed")
        }

        if (!branchRewards.isNullOrEmpty()) {
            if (Objects.nonNull(branchRewards["data"])) {

                val data = branchRewards["data"] as Map<String, Any>
                data.keys.forEach { deviceId ->
                    val device = DeviceMapper(deviceId, mutableListOf())

                    val detail = data[deviceId] as Map<String, Any>
                    detail.keys.forEach { date ->
                        var instStoreAvgValue = 0
                        var accStoreValue = 0.0

                        if (Objects.nonNull(date)) {
                            val reward = detail[date] as Map<String, Any>

                            try {
                                instStoreAvgValue = reward["instStoreAvgValue"] as Int
                            } catch (ex: Exception) {
                                //Empty catch
                            }

                            try {
                                accStoreValue = reward["accStoreValue"] as Double
                            } catch (ex: Exception) {
                                //Empty catch
                            }

                        }

                        device.rewards?.add(RewardMapper(LocalDateTime.parse(date), instStoreAvgValue, accStoreValue))
                    }

                    devices.add(device)
                }
            }
        }

        return devices
    }
}
