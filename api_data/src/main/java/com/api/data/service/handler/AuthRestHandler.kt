package com.api.data.service.handler

import com.api.data.model.deserialize.classes.AuthCreate
import com.api.data.model.deserialize.classes.AuthUpdate
import com.api.data.model.serialize.classes.IDResponse
import com.api.data.model.wrapper.AuthWrapper
import com.api.data.service.service.AuthService
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import java.util.Base64

@Component
class AuthRestHandler @Autowired constructor(private val AUTH_SERVICE: AuthService) {

    private val LOG = LoggerFactory.getLogger(javaClass)

    fun getKeys(authId: Int?, key: String?): AuthWrapper {
        val authList = AUTH_SERVICE.getKeys(authId, key)

        return AuthWrapper(authList)
    }

    fun addKey(create: AuthCreate): IDResponse {
        val formattedString = "${create.userName}:${create.password}"
        val genKey = Base64.getEncoder().encodeToString(formattedString.toByteArray())

        val insertedId = AUTH_SERVICE.addKey(genKey)
        return IDResponse(insertedId)
    }

    fun updateKey(authId: Int, update: AuthUpdate): Boolean {
        val formattedString = "${update.userName}:${update.password}"
        val genKey = Base64.getEncoder().encodeToString(formattedString.toByteArray())

        return AUTH_SERVICE.updateKey(authId, genKey)
    }

    fun deleteKeys(authId: Int?, key: String?): Boolean = AUTH_SERVICE.deleteKeys(authId, key)
}