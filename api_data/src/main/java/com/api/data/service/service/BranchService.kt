package com.api.data.service.service

import com.api.data.advice.exception.BranchNotFoundException
import com.api.data.advice.exception.BrandNotFoundException
import com.api.data.advice.exception.DeviceNotBelongException
import com.api.data.advice.exception.IDNotFoundException
import com.api.data.advice.exception.enumeration.BrandExceptionMessage
import com.api.data.model.entity.BranchEntity
import com.api.data.model.serialize.classes.BranchResponse
import com.api.data.model.serialize.classes.DeviceResponse
import com.api.data.model.serialize.classes.RewardResponse
import com.api.data.repository.AuthRepository
import com.api.data.repository.BranchRepository
import com.api.data.repository.BrandRepository
import com.api.data.repository.DeviceRepository
import com.api.data.repository.RewardRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
open class BranchService @Autowired constructor(
        private val AUTH_REPOSITORY: AuthRepository,
        private val BRAND_REPOSITORY: BrandRepository,
        private val BRANCH_REPOSITORY: BranchRepository,
        private val DEVICE_REPOSITORY: DeviceRepository,
        private val REWARD_REPOSITORY: RewardRepository
) {

    fun getBranch(authKey: String?, branchId: Int?, branchName: String?): List<BranchResponse> {
        val authEntity = AUTH_REPOSITORY.getKeys(key = authKey)
        if (authEntity.isEmpty()) {
            throw IDNotFoundException()
        }

        val brandEntity = BRAND_REPOSITORY.getBrand(authId = authEntity[0].id)
        if (brandEntity.isEmpty()) {
            throw BrandNotFoundException(messageId = BrandExceptionMessage.NO_BRAND_FOR_THIS_AUTH)
        }

        return BRANCH_REPOSITORY.getBranch(branchId, branchName, brandEntity[0].id).map {
            BranchResponse(it.id!!, it.name!!)
        }
    }

    fun addBranch(authKey: String, branchId: Int, branchName: String): Boolean {
        val authList = AUTH_REPOSITORY.getKeys(key = authKey)
        if (authList.isEmpty()) {
            throw IDNotFoundException()
        }

        val brandList = BRAND_REPOSITORY.getBrand(authId = authList[0].id!!)
        if (brandList.isEmpty()) {
            throw BrandNotFoundException(messageId = BrandExceptionMessage.NO_BRAND_FOR_THIS_AUTH)
        }

        return BRANCH_REPOSITORY.addBranch(BranchEntity(branchId, branchName, brandList[0].id))
    }

    fun getDevice(authKey: String, branchId: Int): List<DeviceResponse> {
        val branches = getBranch(authKey, null, null)
        if (branches.isEmpty()) {
            throw IDNotFoundException()
        }

        if (branches.none { it.branchId == branchId }) {
            throw BranchNotFoundException()
        }

        return DEVICE_REPOSITORY.getDevice(branchId = branchId).map {
            DeviceResponse(it.id!!)
        }
    }

    fun getReward(authKey: String, branchId: Int, deviceId: String): List<RewardResponse> {
        val devices = getDevice(authKey, branchId)

        if (devices.none { it.deviceId == deviceId }) {
            throw DeviceNotBelongException()
        }

        return REWARD_REPOSITORY.getReward(deviceId = deviceId).map {
            RewardResponse() + it
        }
    }
}
