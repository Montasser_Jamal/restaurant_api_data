CREATE TABLE IF NOT EXISTS `reward` (
  `id` int(10) NOT NULL PRIMARY KEY AUTO_INCREMENT,
  `date` datetime NOT NULL UNIQUE KEY,
  `inst_store_avg_value` int(10) NOT NULL,
  `acc_store_value` double(10,6) NOT NULL,
  `device_id` varchar(20) NOT NULL,
  CONSTRAINT `reward_ibfk_1` FOREIGN KEY (`device_id`) REFERENCES `device` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;