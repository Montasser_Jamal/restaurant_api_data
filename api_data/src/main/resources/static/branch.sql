CREATE TABLE IF NOT EXISTS  `branch` (
  `id` int(10) NOT NULL PRIMARY KEY,
  `name` varchar(50) NOT NULL,
  `brand_id` int(10) NOT NULL,
  CONSTRAINT `branch_ibfk_1` FOREIGN KEY (`brand_id`) REFERENCES `brand` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;